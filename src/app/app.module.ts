import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { BurgerFillingComponent } from './burger-filling/burger-filling.component';

@NgModule({
  declarations: [
    AppComponent,
    IngredientComponent,
    BurgerFillingComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
