import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent {
  @Input() name = '';
  @Input() count = 0;
  @Output() add = new EventEmitter();
  @Output() delete = new EventEmitter();

  onClickAdd() {
    this.add.emit();
  };

  onClickDelete() {
    this.delete.emit();
  };

}
