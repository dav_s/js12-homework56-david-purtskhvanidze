import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-burger-filling',
  templateUrl: './burger-filling.component.html',
  styleUrls: ['./burger-filling.component.css']
})
export class BurgerFillingComponent {
  @Input() className = '';
  @Input() count = 0;

}

