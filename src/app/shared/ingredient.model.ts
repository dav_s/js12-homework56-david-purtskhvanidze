export class Ingredient {
  constructor(
    public name: string,
    public count: number,
    public price: number
  ) {}

  getPrice() {
    return this.count * this.price;
  }
}
