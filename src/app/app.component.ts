import { Component } from '@angular/core';
import { Ingredient } from "./shared/ingredient.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ingredients: Ingredient[] = [
    new Ingredient('Meat', 0, 50),
    new Ingredient('Cheese', 0, 20),
    new Ingredient('Salad', 0, 5),
    new Ingredient('Bacon', 0, 30),
  ];

  addIngredient(index: number) {
    this.ingredients[index].count = this.ingredients[index].count + 1;
  };

  deleteIngredient(index: number) {
    if (this.ingredients[index].count > 0) {
      this.ingredients[index].count = this.ingredients[index].count - 1;
    }
  };


}
